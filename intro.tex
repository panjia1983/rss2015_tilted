\section{Introduction}
\label{sec:intro}

Grasping and manipulation are two main functions for a robot 
to rearrange the world around it. One typical robotic grasp 
and manipulation task is the pick-and-place operation, 
where the robot picks up an user-specified object at a given 
pose and places it at a target pose. However, 
such a pick-and-place operation may not be able to be performed 
due to kinematic constraints of the robotic arm or collisions 
between the robot and its surrounding environment. One solution
to this problem is pick-and-place regrasp. 
Namely, two adjacent pick-and-place operations are joined by one
placement during the grasping: after the object is picked up by the first grasp,
it is placed on a stable location and then be picked up again using another
grasp. To achieve a successful pick-and-place operation, one or more stable
locations may be necessary for the intermediate placement. Pick-and-place
regrasp has been extensively studied by a large amount of
work~\cite{Lozano-Perez:1992:HRT,Terasaki:1998:MPI} since 1980s, due to its
importance for single arm assembly tasks. Most previous work assumed
intermediate locations to be flat, e.g., a table surface or a ground, and
focused on computing a feasible or optimal robotic trajectory in the
high-dimensional configuration space for accomplishing pick-and-place tasks.
However, workbenches with tilted surfaces have shown to be useful for improving
human being's manipulation experiences, because some objects can be better
manipulated on a tilted surface. For instance, the shelf in 
Figure~\ref{fig:tilted}(a) has a combination of two
surfaces, one horizontal and one tilted. Such a design enables both the
convenient grasping of lotion-squeeze containers and the convenient
manipulation of lotion-pump bottles. A similar combination of horizontal and
tilted surfaces can be found in the labor-intensive factories in East Asia
(Figure~\ref{fig:tilted}(b)). They are designed to improve the efficiency of
production pipelines. Whereas tilted surfaces are popular to human manipulation,
robotic manipulation rarely benefits from the exploitation of titled surfaces.

\begin{figure}[!htbp]
	\centering
	\subfloat[A storage shelf] {
		\centering
		\includegraphics[height=1.3in]{figs/humanexam2.png}
		\label{fig:tilted:storage}
	}
	\subfloat[Production pipelines in East Asia] {
		\includegraphics[height=1.3in]{figs/humanexam.png}
		\label{fig:tilted:asia}
	}
	\caption{A combination of horizontal and tilted surfaces is popular in
	domestic workbenches such as storage shelves and manual production 
	pipelines; and this improves human being's experience in grasping and
	manipulation.}
	\label{fig:tilted}
\end{figure}

In this paper, we present an algorithm to compute the optimal solution for a
workcell surface's location and tilt angle, for maximizing the pick-and-place
capability of a given object and a given robotic arm. Our
algorithm uses two heuristics. The first heuristic is the stable placements of
the given object. This heuristic is inherent to the shape of a specific object.
The second heuristic is the manipulability of the given robotic arm. This
heuristic is inherent to the mechanical configuration of a specific manipulator.
These two heuristics encode both the constraints induced by the
robot and the object, and act as a guide for optimizing the
position $(x, y, z)$ and the pitch and yaw angles $(\theta, \beta)$ of a tilted
surface. 

In particular, we first generate random samples for the five parameters $(x, y,
z, \theta, \beta)$ of a titled surface. For each five-dimensional sample, we
evaluate the stability when an object is placed on the slant, and the
manipulability of a robotic arm while executing a grasp with the gripper. 
Given all the samples and their evaluation results, we use regression
techniques to estimate functions describing the stability and 
manipulability for the entire configuration space. 
We then formulate the pick-and-place capability as a function 
of the stability and manipulability, and compute the tilted surface's 
optimal location and tilt angles by optimizing the pick-and-place capability 
objective. We expect it to play some essential role in the 
upcoming next-generation manufacturing system.

% \textcolor{blue}{Our results show that tilted work surface 
% could be beneficial for robotic manipulation, by improving 
% the completeness, the cost, and the length of regrasp sequences.}







