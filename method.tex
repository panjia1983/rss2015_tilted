\section{Tilted Surface Design}

In this section, we first discuss the stability and manipulability
measurements for a tilted surface, and then optimize the tilted surface
configuration according to these two measurements.

\subsection{Stability}

The stability of an object's placement on a tilted surface is
determined by three factors: the surface's pitch angle $\theta$,
the object's rotation around the surface normal $\alpha$,
and the region of support between the object and the tilted surface,
where $\theta$ and $\alpha$ are shown in Figure~\ref{fig:placement}.
The region of support is determined by first computing
the convex hull for the object's mesh model, and then clustering the mesh
triangles and choosing one face of the clusters as the supporting face
between the object and the tilted surface \cite{Harada:2014:JRS}.
The supporting face's projection onto the horizontal plane is the region of
support. Given an object $O$, a region of support $f$,
and an assignment of $\theta$ and $\alpha$,
we can evaluate the object's stability by projecting
its center of mass onto $f$. If the projection is inside
the region of support, the object stays stable on the slant;
otherwise, it is unstable.

We compute a continuous function for describing an object's stability on a
tilted surface by performing regression on random samples.
In particular, given an object $O$, we fix its region of support $f$
and randomly sample many configurations of $\theta$ and $\alpha$.
After evaluating the stability for each sampled items, we use
the regression technique to estimate a stability function $h_1^{O, f}$：
\begin{equation}
h_1^{O,f}(\theta, \alpha) = \left\{
  \begin{array}{l l}
    1 & \quad \text{if $O$ is stable}\\
    0 & \quad \text{if $O$ is unstable}
  \end{array} \right.
\end{equation}
The regression result $h_1^{O, f}(\theta, \alpha)$ provides a
compact representation for evaluating an object's stability
on a slant under a given configuration. However, while
using a tilted surface for regrasping, we hope the object to
stay stable under many different values of $\alpha$ and $f$,
which can increase the number of candidate placements and
improves the flexibility of the regrasping strategy.
For this purpose, we compute a function $h_1^O$ to
count the number of $\alpha$ assignments
that could result in a stable placement:
\begin{equation}
h_1^{O}(\theta) = \sum_{f \in F}(\int_{\alpha \in [0, 2 \pi]}
h_1^{O,f}(\theta, \alpha)),
\end{equation}
where $F$ is the set of mesh clusters computed from the convex hull.

\subsection{Manipulability}

The manipulability of a robotic arm describes its ability
to move freely in all directions in the workspace.
When a manipulator is at a singular configuration,
there are directions of movement which require high
joint rates and forces. Near a sigularity,
the movement may also be difficult in certain directions.
There are many different manipulability measures that
have been proposed in the literature, which are useful in
different situations. In our approach, we adopt the measurement
proposed by Yoshikawa~\cite{Yoshikawa:1985:MRM}:
$\mu(\mathbf q) = \sqrt{\Det(\mathbf J \mathbf J^T)}$,
where $\mathbf J = \mathbf J(\mathbf q)$ is Jacobian matrix
of the robotic arm at joint configuration $\mathbf q$.

Since we don't have any apriori information about
the tasks to be executed, we use the most approachable
configuration of the gripper, which is determined
by the pose of the object standing on the tilted
surface, to evaluate the manipulability for a given slant
configuration. In particular, given an assignment of
the five-dimensional tilted surface parameter
$(x,y,z,\theta, \beta)$ and the scalar object orientation
parameter $\alpha$, the object's pose can
be formalized by a homogeneous matrix:

\begin{equation}
\left[ \begin{array}{cc}
R_z(\beta)R_y(\theta) R_z(\alpha) & [x,y,z]^T \\
0 & 1 \end{array} \right],
\end{equation}

where $R_z$ is the matrix for rotation around
$z$-axis and $R_y$ is the matrix for rotation around $y$-axis.

Given this pose matrix, we then compute the robot's joint angle
$\mathbf q$ using inverse kinematics, and then the corresponding
manipulability value $\mu(\mathbf q)$. We also denote
$\mu(\mathbf q)$ as $\mu(x,y,z,\theta, \beta, \alpha)$ to
indicate that the manipulability is a function of
system parameters $(x,y,z,\theta, \beta, \alpha)$.

Similar to the stability, we also calculate a
continuous function for describing a robotic arm's
manipulability by regression on random samples
of $(x, y, z, \theta, \alpha, \beta)$:

\begin{equation}
h_2(x, y, z, \theta, \beta, \alpha) = \mu(x,y,z,\theta, \beta, \alpha).
\end{equation}

In order to analyze the influence of different
configurations of the tilted surface on the manipulability,
we also summarize the manipulability function $h_2$ over
the $\alpha$ parameter, similar to what we did for
the stability. In particular, we define

\begin{equation}
h_2^{\text{min}}(x,y,z,\theta,\beta) = \min_{\alpha} \mu(x,y,z,\theta, \beta,\alpha)
\end{equation}
and
\begin{equation}
h_2^{\text{max}}(x,y,z,\theta,\beta) = \max_{\alpha} \mu(x,y,z,\theta, \beta,\alpha),
\end{equation}
where $h_2$ ($h_2^{\text{min}}$ or $h_2^{\text{max}}$) provides the pessimistic (optimistic) estimation for
the overall manipulability of the tilted surface.

\subsection{Tilted Surface Optimization}

The optimal configuration of a tilted surface for the grasping and manipulation
of a specific object $O$ can be computed by maximizing 
\begin{equation}
    f^O(x, y, z, \theta, \beta) = h_1^O(\theta) \times
    h_2(x,y,z,\theta,\beta),
\end{equation}
i.e., the optimal slant should provide a high stability and a high manipulability for this object. 

We solve this optimization problem in three steps. First, we ignore $h_2$ and compute the optimal pitch angle as
$\hat{\theta} = \argmax_{\theta} h_1^O(\theta)$. Next, we fix $\theta$ at $\hat{\theta}$ and optimize the other 
variables $(\hat{x}, \hat{y}, \hat{z}, \hat{\beta}) = \argmax_{x, y, z,\beta} h_2(x,y,z, \hat{\theta}, \beta)$. 
Finally, we use $(\hat{x}, \hat{y}, \hat{z}, \hat{\theta}, \hat{\beta})$ as the initial guess to optimize 
$f^O(x, y, z, \theta, \beta)$ using the gradient decent algorithm, and the result $(x^O, y^O, z^O, \theta^O,
 \beta^O)$ is the optimized titled surface configuration for a specific object $O$. 

We can further extend the algorithm to a set of objects. In particular, given a set of object $\{O_i\}$, 
we change the objective function to
\begin{equation}
    \tilde{f} = \prod_{O_i} f^{O_i},
\end{equation}
and the optimization process is the same.

